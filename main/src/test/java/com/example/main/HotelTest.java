package com.example.main;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
public class HotelTest {

    @Test
    public void testCreateRooms() {
        Hotel hotel = new Hotel();
        assertTrue(hotel.getRooms().contains(100));
        assertEquals(12, hotel.getRooms().size());
    }

    @Test
    public void testCorrectCheckin() {
        Hotel hotel = new Hotel();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        ByteArrayInputStream input = new ByteArrayInputStream("104\r\nMichał\r\n".getBytes());
        System.setIn(input);

        hotel.checkin();

        String expected = """
                Podaj numer pokoju: \r
                Podaj imię gościa: \r
                Dodano Michał do pokoju o numerze 104\r
                """;
        assertEquals(expected, output.toString());
    }

    @Test
    public void testWrongNumberCheckin() {
        Hotel hotel = new Hotel();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        ByteArrayInputStream input = new ByteArrayInputStream("-1\r\nMichał\r\n".getBytes());
        System.setIn(input);

        hotel.checkin();

        String expected = """
                Podaj numer pokoju: \r
                Pokój o numerze -1 nie istnieje\r
                """;
        assertEquals(expected, output.toString());
    }
    @Test
    public void testCorrectCheckout() {
        Hotel hotel = new Hotel();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        ByteArrayInputStream input = new ByteArrayInputStream("104\r\nMichał\r\n".getBytes());
        System.setIn(input);

        hotel.checkin();

        input = new ByteArrayInputStream("104\r\n".getBytes());
        System.setIn(input);

        hotel.checkout();
        String expected = """
                Podaj numer pokoju: \r
                Podaj imię gościa: \r
                Dodano Michał do pokoju o numerze 104\r
                Podaj numer pokoju: \r
                Usunięto Michał z pokoju o numerze 104\r
                """;
        assertEquals(expected, output.toString());
    }

    /*@Test
    public void testList() {
        Hotel hotel = new Hotel();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        hotel.list();
        String expected = "Lista pokoi: \r\n100 101 102 103 104 105 106 107 108 109 110 111 \r\n";
        assertEquals(expected, output.toString());
    }*/

    @Test
    public void testView() {
        Hotel hotel = new Hotel();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        ByteArrayInputStream input = new ByteArrayInputStream("104\r\nMichał\r\n".getBytes());
        System.setIn(input);

        hotel.checkin();

        input = new ByteArrayInputStream("104\r\n".getBytes());
        System.setIn(input);

        hotel.view();
        String expected = """
                Podaj numer pokoju: \r
                Podaj imię gościa: \r
                Dodano Michał do pokoju o numerze 104\r
                Podaj numer pokoju: \r
                Informacje o pokoju\r
                - numer pokoju: 104\r
                - opis: Luksusowy pokój 1-osobowy\r
                - cena: 500\r
                - mieszkaniec: Michał\r
                """;
        assertEquals(expected, output.toString());
    }
}
