package com.example.main;

public class Room {
    private String guestName = "";
    private int cena = 500;
    private String opis = "Luksusowy pokój 1-osobowy";

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String name) {
        this.guestName = name;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
}
