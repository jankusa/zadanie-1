package com.example.main;

import com.example.utils.MyMap;

import java.io.*;
import java.util.List;
import java.util.Scanner;

public class Hotel {
    private final MyMap<Integer, Room> rooms;

    public MyMap<Integer, Room> getRooms() {
        return rooms;
    }

    public Hotel() {
        this.rooms = new MyMap<>();
        createRooms();
    }

    public Hotel(String file) {
        // String file = "src\\roomsInput.csv";
        this.rooms = new MyMap<>();
        BufferedReader reader = null;
        String line = "";
        try {
            reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null) {
                String[] row = line.split(",");
                Room newRoom = new Room();
                newRoom.setOpis(row[1]);
                try{
                    newRoom.setCena(Integer.parseInt(row[2]));
                }
                catch (NumberFormatException ex){
                    System.out.println(ex.getMessage());
                }
                if(row.length == 4) {
                    newRoom.setGuestName(row[3]);
                }
                try{
                    rooms.put(Integer.parseInt(row[0]), newRoom);
                }
                catch (NumberFormatException ex){
                    System.out.println(ex.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                assert reader != null;
                reader.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
    public void save() throws FileNotFoundException {
        String fileName = "saved.csv";
        PrintWriter pw = new PrintWriter(new File(fileName));
        for(int roomNumber : rooms.keys()) {
            pw.printf("%s,%s,%d,", roomNumber, rooms.get(roomNumber).getOpis(), rooms.get(roomNumber).getCena());
            if(!rooms.get(roomNumber).getGuestName().isEmpty()) {
                pw.print(rooms.get(roomNumber).getGuestName() + ",");
            }
        }
        pw.close();
        System.out.println("Zapisano dane do pliku " + fileName);
    }
    public void createRooms() {
        for(int i = 0; i < 12; i++) {
            Room room = new Room();
            rooms.put(100+i, room);
        }
    }

    public void list() {
        List<Integer> roomNumbers = rooms.keys();
        System.out.println("Lista pokoi: ");
        for(int number : roomNumbers) {
            System.out.print(number + " ");
        }
        System.out.print("\n");
    }

    public void view() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer pokoju: ");
        int numer = scanner.nextInt();
        scanner.nextLine();
        Room room = rooms.get(numer);
        if(room == null) {
            System.out.println("Pokój o numerze " + numer + " nie istnieje");
            return;
        }
        System.out.println("Informacje o pokoju");
        System.out.println("- numer pokoju: " + numer);
        System.out.println("- opis: " + room.getOpis());
        System.out.println("- cena: " + room.getCena());
        System.out.println("- mieszkaniec: " + room.getGuestName());
    }

    public void checkin() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer pokoju: ");
        int numer = scanner.nextInt();
        scanner.nextLine();
        Room room = rooms.get(numer);
        if(room == null) {
            System.out.println("Pokój o numerze " + numer + " nie istnieje");
            return;
        }
        if(!room.getGuestName().isEmpty()) {
            System.out.println("Pokój o numerze " + numer + " jest zajęty");
            return;
        }
        System.out.println("Podaj imię gościa: ");
        String name = scanner.nextLine();
        room.setGuestName(name);
        System.out.println("Dodano " + name + " do pokoju o numerze " + numer);
    }

    public void checkout() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer pokoju: ");
        int numer = scanner.nextInt();
        scanner.nextLine();
        Room room = rooms.get(numer);
        if(room == null) {
            System.out.println("Pokój o numerze " + numer + " nie istnieje");
            return;
        }
        if(room.getGuestName().isEmpty()) {
            System.out.println("Pokój o numerze " + numer + " jest pusty");
            return;
        }
        System.out.println("Usunięto " + room.getGuestName() + " z pokoju o numerze " + numer);
        room.setGuestName("");
    }

    public static void main(String[] args) throws FileNotFoundException {
        Room room = new Room();
        Scanner scanner = new Scanner(System.in);
        Hotel hotel = new Hotel("main\\src\\roomsInput.csv");
        boolean repeat = true;
        System.out.println("Witaj w Kapitolu");
        while(repeat) {
            String command = scanner.nextLine().toLowerCase();
            switch (command) {
                case "list":
                    hotel.list();
                    break;
                case "checkin":
                    hotel.checkin();
                    break;
                case "checkout":
                    hotel.checkout();
                    break;
                case "view":
                    hotel.view();
                    break;
                case "save":
                    hotel.save();
                    break;
                case "exit":
                    repeat = false;
                    break;
                default:
                    System.out.println("Komenda \"" + command + "\" nie istnieje");
            }
        }
        System.out.println("Zakończono program");
    }
}
