package com.example.utils;

import java.util.*;

interface Map<K, V> {
    /**
     * Dodanie elementu do mapy pod podanym kluczem.
     Jeśli podany klucz istnieje to metoda powinna podmienić wartość.
     * @param key klucz (nie null)
     * @param value wartość kryjącą się pod kluczem (nie null)
     * @return true jeśli się uda dodać element, false jeśli nie
     */
    boolean put(K key, V value);
    /**
     * Usunięcie podanego klucza oraz wartości, która jest przechowywana pod tym
     kluczem.
     * @param key klucz do usunięcia
     * @return true jeśli uda się usunać klucz, false w przeciwnym przypadku
     */
    boolean remove(K key);
    /**
     * Zwraca wartość pod podanym kluczem lub null jeśli nie ma podanego klucza.
     * @param key klucz (nie ull)
     * @return wartość pod kluczem lub null jeśli nie ma wartości dla podanego klucza
     */
    V get(K key);
    /**
     * Zwraca listę kluczy
     * @return java.util.List lista kluczy
     */
    List<K> keys();
    /**
     * Sprawdza czy podany klucz istnieje w mapie.
     * @param key wartość klucza do sprawdzenia.
     * @return true jeśli klucz istnieje.
     */
    boolean contains(K key);

    int size();
}

public class MyMap<K, V> implements Map<K, V>{
    private final List<K> keys;
    private final List<V> values;

    public MyMap() {
        this.keys = new ArrayList<>();
        this.values = new ArrayList<>();
    }

    @Override
    public boolean put(K key, V value) {
        if (key == null || value == null) {
            return false;
        }
        int index = keys.indexOf(key);
        if (index != -1) {
            values.set(index, value);
        } else {
            keys.add(key);
            values.add(value);
        }
        return true;
    }

    @Override
    public boolean remove(K key) {
        for(K k : keys) {
            if(k == key) {
                values.remove(keys.indexOf(k));
                keys.remove(k);
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(K key) {
        for(K k : keys) {
            if(k == key) {
                return values.get(keys.indexOf(k));
            }
        }
        return null;
    }

    @Override
    public List<K> keys() {
        return keys;
    }

    @Override
    public boolean contains(K key) {
        return keys.contains(key);
    }

    @Override
    public int size() {
        return keys.size();
    }

}