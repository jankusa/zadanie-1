package com.example.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyMapTest {

    @Test
    public void testPutAndGet() {
        MyMap<String, Integer> myMap = new MyMap<>();
        assertTrue(myMap.put("jeden", 1));
        assertTrue(myMap.put("dwa", 2));

        assertEquals(1, (int) myMap.get("jeden"));
        assertEquals(2, (int) myMap.get("dwa"));
        assertNull(myMap.get("trzy"));
    }

    @Test
    public void testPutUpdatesValue() {
        MyMap<String, Integer> myMap = new MyMap<>();
        assertTrue((myMap.put("jeden", 11)));
        assertTrue((myMap.put("jeden", 1)));

        assertEquals(1, (int) myMap.get("jeden"));
    }

    @Test
    public void testRemove() {
        MyMap<String, Integer> myMap = new MyMap<>();
        assertTrue(myMap.put("jeden", 1));
        assertTrue(myMap.put("dwa", 22));

        assertTrue(myMap.remove("dwa"));
        assertNull(myMap.get("dwa"));

        assertTrue(myMap.put("dwa", 2));
        assertEquals(2, (int) myMap.get("dwa"));
    }

    @Test
    public void testContains() {
        MyMap<String, Integer> myMap = new MyMap<>();
        assertTrue(myMap.put("jeden", 1));
        assertTrue(myMap.put("dwa", 2));

        assertTrue(myMap.contains("dwa"));
        assertFalse(myMap.contains("trzy"));
    }

    @Test
    public void testKeys() {
        MyMap<String, Integer> myMap = new MyMap<>();
        assertTrue(myMap.put("jeden", 1));
        assertTrue(myMap.put("dwa", 2));

        assertEquals(2, myMap.keys().size());
        assertTrue(myMap.keys().contains("jeden"));
        assertFalse(myMap.keys().contains("trzy"));
    }
}